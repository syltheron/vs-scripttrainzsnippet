# Change Log

All notable changes to the "scripttrainzsnippet" extension will be documented in this file.

## [Unreleased]

## [0.0.6.0]

[Added]

- Ajout de la classe `Browser`
- Ajout de la classe `Constructor`

## [0.0.4.1]

[Added]

- Ajout de fonction dans la classe `ASSET`
- Ajout de la classe `GameObjectId`
- Ajout de la classe `GameObject`

[Modified]

- Modification des liens de documentation avec l'api `https://api.trainznation.eu`

## [0.0.3]

[Added]

- Support la class Asset

[Modified]

- Incrémentation des valeurs de script 'Mot Clé'

## [0.0.2]

[modified]

- Couverture des scripts GS

## [0.0.1]

[Added]

- Supporte les Mots Clé (Keyword)