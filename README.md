Bibliothèque API des scripts de Trainz (GS)
===============================================================

**VsCode Version:** 1.33.0 ou sup  
**Version de l'extension:** 0.0.6.0

Permet d'autocompleter les informations de la bibliothèque API des scripts de Trainz.

## Etape de Création

- Créer une fichier .gs vide
- Taper la commande `genScript` pour générer le code basique
- Taper la fonction que vous voulez editer à l'intérieur du code basique.
